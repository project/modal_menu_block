<?php

namespace Drupal\modal_menu_block\Plugin\Derivative;

use Drupal\system\Plugin\Derivative\SystemMenuBlock;

/**
 * Provides modal menu block plugin definitions.
 */
class ModalMenuBlock extends SystemMenuBlock {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    parent::getDerivativeDefinitions($base_plugin_definition);
    foreach ($this->derivatives as $menu => $definition) {
      $this->derivatives[$menu]['admin_label'] = $this->derivatives[$menu]['admin_label'] . ' (Modal)';
    }
    return $this->derivatives;
  }

}
