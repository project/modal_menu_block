<?php

namespace Drupal\modal_menu_block\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Plugin\Block\SystemMenuBlock;

/**
 * Provides a modal menu block.
 *
 * @Block(
 *   id = "modal_menu_block_modal_menu",
 *   admin_label = @Translation("Modal Menu"),
 *   category = @Translation("Menus"),
 *   deriver = "Drupal\modal_menu_block\Plugin\Derivative\ModalMenuBlock",
 * )
 */
class ModalMenuBlock extends SystemMenuBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'trigger_text' => $this->t('Menu'),
      'expandable' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['trigger_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trigger element text'),
      '#default_value' => $this->configuration['trigger_text'] ?? NULL,
      '#required' => TRUE,
    ];

    $form['expandable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expandable'),
      '#description' => $this->t('Apply expandable JS to menu displayed in modal.'),
      '#default_value' => $this->configuration['expandable'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['trigger_text'] = $form_state->getValue('trigger_text');
    $this->configuration['expandable'] = $form_state->getValue('expandable');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'modal_menu_block',
      '#menu_name' => $this->getDerivativeId(),
      '#trigger' => $this->configuration['trigger_text'],
      '#content' => parent::build(),
      '#expandable' => $this->configuration['expandable'],
    ];
  }

}
