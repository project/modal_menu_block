(function (Drupal, $) {

  Drupal.behaviors.modalMenuBlock = {
    attach: function (context) {
      const menus = once('modalMenuBlock', '.modal-menu-block');
      menus.forEach(menu => {
        const $menu = $(menu);
        const $content = $('.modal-menu-block--content', $menu);

        if ($content.length === 0) {
          return;
        }

        // Reference the modal content back to the menu.
        $content[0].modalMenu = $menu;
        let dialog = Drupal.dialog($content[0], {});
        $('.modal-menu-block--trigger', $menu).click(() => {
          dialog.showModal();
          $menu.addClass('open');
        });

        // When the modal is closed, remove the "open" class from the menu
        // wrapper.
        $(window).on('dialog:afterclose', (event, closedModal, el) => {
          el.each((idx, content) => {
            if (content.hasOwnProperty('modalMenu')) {
              content.modalMenu.removeClass('open');
            }
          });
        });
      });
    }
  };

})(Drupal, jQuery)
