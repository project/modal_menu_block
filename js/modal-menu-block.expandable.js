(function (Drupal, $) {

  Drupal.behaviors.modalMenuBlockExpandable = {
    attach: function (context) {
      const menus = once('modalMenuBlockExpandable', '.modal-menu-block.expandable');

      // Attempt to add an opener for each UL.
      menus.forEach(menu => {
        $('ul', menu).each((idx, item) => {
          // Find the nearest ancestor LI.
          const parent = $(item).parents('li').get(0);
          if (!parent) {
            return;
          }

          // Add the opener.
          const $opener = $('<a class="modal-menu-block--opener"></a>');
          $(item).before($opener);

          // Bind the opener's click event.
          $opener.click(event => {
            event.preventDefault();
            Drupal.modalMenuBlockExpandable.openerClicked($opener.get(0), item, parent);
          });
        });
      });
    }
  };

  /**
   * This code is split out from the behavior to allow the expand/collapse to
   * be easily overridden by a theme, etc.
   */
  Drupal.modalMenuBlockExpandable = {
    collapseChildren: function (child) {
      this.defaultCollapseExpand(child, false);
    },
    expandChildren: function (child) {
      this.defaultCollapseExpand(child, true);
    },
    openerClicked: function (opener, child, parent) {
      const $opener = $(opener);
      const $parent = $(parent);

      if ($parent.hasClass('expanded')) {
        this.collapseChildren(child, parent);
        $opener.toggleClass('expanded', false);
        $parent.toggleClass('expanded', false);
      }
      else {
        this.expandChildren(child, parent);
        $opener.toggleClass('expanded', true);
        $parent.toggleClass('expanded', true);
      }
    },
    defaultCollapseExpand: function (child, state) {
      $(child).slideToggle(state);
    }
  };

})(Drupal, jQuery)
