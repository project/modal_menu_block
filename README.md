# Modal Menu Block

Provides a block to display a menu within a modal. Modal Menu Block is
intentionally non-prescriptive about styling so that customization can be done
within a site's theme.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/modal_menu_block).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/modal_menu_block).


## Table of contents

- Requirements
- Installation
- Configuration
- FAQ
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module has no menu, modifiable settings, or configuration. When enabled,
it adds the option to place a menu block by going to Structure >> Block Layout.
The description of the provided block takes the format "[Menu Name] (Modal)"
within the Menus category.


## FAQ

**Q: How can I customize a modal menu block within my site's theme?**

**A:** Modal Menu Block provides the following classes for the HTML
it produces:
- "modal-menu-block" and "modal-menu-block--MenuName" on the container holding
the menu and trigger. The class "open" is also added and removed when the modal
is opened and closed.
- "modal-menu-block--trigger" on the `<span>` that opens the modal menu.
- "modal-menu-block--content" on the menu's container.

When the Expandable option is turned on in the block's configuration, the
following classes are added:
- "expandable" on the container holding the menu and trigger
- "expanded" on a parent menu's container (usually a `<li>`) when it's open
- Parent menu containers gain a link to open and close them with the class
"modal-menu-block--opener". This link also gets the "expanded" class when it's
opened.

A modal menu's expanding behavior can also be overriden by a theme.


## Maintainers

- Will Long - [kerasai](https://www.drupal.org/u/kerasai)
- Kristin Wiseman - [kwiseman](https://www.drupal.org/u/kwiseman)
