<?php

/**
 * @file
 * Theme hooks for Modal Menu Block module.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_modal_menu_block(&$variables) {
  $variables['#attached']['library'][] = 'modal_menu_block/modal_menu_block';
  $variables['trigger'] = $variables['modal_menu']['#trigger'];
  $variables['content'] = $variables['modal_menu']['#content'];
  $variables['attributes']['class'][] = Html::getClass('modal-menu-block');
  $variables['attributes']['class'][] = Html::getClass('modal-menu-block--menu-' . $variables['modal_menu']['#menu_name']);
  if (!empty($variables['modal_menu']['#expandable'])) {
    $variables['attributes']['class'][] = 'expandable';
    $variables['#attached']['library'][] = 'modal_menu_block/modal_menu_block.expandable';
  }
}
